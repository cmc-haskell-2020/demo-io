{-# LANGUAGE RecordWildCards #-}
module DemoIO
    ( run
    ) where

import System.Random
import System.IO

import Graphics.Gloss.Interface.Pure.Game

--------------
-- Data types.
--------------

-- Config for colors.
data ColorConfig = ColorConfig
  { color1 :: Color -- Color for first number.
  , color2 :: Color -- Color for second number.
  }

-- General application state.
data AppState = AppState
  { colors    :: ColorConfig -- Colors config.
  , randomGen :: StdGen      -- Random number generator.
  , number    :: Int         -- Current number.
  }

-------------
-- Constants.
-------------

-- Path to config file.
configPath :: FilePath
configPath = "config.txt"

-- Random numbers range.
range :: (Int, Int)
range = (-10, 10)

-- Game display mode.
display :: Display
display = FullScreen

-- Background color.
bgColor :: Color
bgColor = greyN 0.3

-- Simulation steps per second.
fps :: Int
fps = 60

-- Text shift on screen.
textShift :: Float
textShift = 250

------------------
-- Pure functions.
------------------

-- Parse config from string.
-- Config format: 2 lines, one color per line.
parseConfig :: String -> Maybe ColorConfig
parseConfig str = case map findColor (lines str) of
  [Just c1, Just c2] -> Just (ColorConfig c1 c2)
  _ -> Nothing
  where
    findColor :: String -> Maybe Color
    findColor s = lookup s colorMap
    colorMap = zip names colors
    colors = [red, green, blue, black]
    names  = ["red", "green", "blue", "black"]

-- Draw a picture: two numbers of different colors defined in config.
drawApp :: AppState -> Picture
drawApp (AppState ColorConfig{..} _ n) = Pictures [pic1, pic2]
  where
    txt = Text (show n)
    pic1 = Translate (-textShift) 0 $ Color color1 txt
    pic2 = Translate textShift 0 $ Color color2 txt

-- Handle events.
handleEvent :: Event -> AppState -> AppState
-- Generate new random number when Space is pressed.
handleEvent (EventKey (SpecialKey KeySpace) Down _ _) AppState{..} =
  -- Get new random number and generator.
  let (newn, newGen) = randomR range randomGen
  -- Update BOTH number AND generator.
  in AppState colors newGen newn
-- Ignore all other events.
handleEvent _ state = state

-- Simulation step (updates nothing).
updateApp :: Float -> AppState -> AppState
updateApp _ x = x

------------------------------
-- Main function for this app.
------------------------------

-- Run game. This is the ONLY unpure function.
run :: IO ()
run = do
  -- Load config file contents (unpure action).
  str <- readFile configPath
  -- Try to parse config.
  case parseConfig str of
    Nothing -> putStrLn "Parse error"
    Just cfg -> do
      -- Get new random number generator (unpure action).
      gen <- getStdGen
      let initState = AppState cfg gen 0
      -- Run application.
      play display bgColor fps initState drawApp handleEvent updateApp
